import { Component } from '@angular/core';
import { AppService } from './app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'aula02-view';

  nome: string;
  valor: number;

  produto = {nome: "", valor: 0.0};

  constructor(private appService: AppService){}

  salvar(){
    this.appService.salvar(this.produto).subscribe();
    this.produto = {nome: "", valor: 0.0};
    console.log(`executou! ${this.produto.nome} ${this.produto.valor}`);
  }

}
